import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Billdetail } from 'src/bills/entities/bill-detail';
import { User } from 'src/users/entities/user.entity';
import { Material } from 'src/material/entities/material.entity';
import { Repository } from 'typeorm';
import { CreateBillDto } from './dto/create-bill.dto';
import { UpdateBillDto } from './dto/update-bill.dto';
import { Bill } from './entities/bill.entity';
@Injectable()
export class BillService {
  constructor(
    @InjectRepository(Bill)
    private billRepository: Repository<Bill>,
    @InjectRepository(User)
    private employeeRepository: Repository<User>,
    @InjectRepository(Billdetail)
    private billDetailRepository: Repository<Billdetail>,
    @InjectRepository(Material)
    private materialRepository: Repository<Material>,
  ) {}
  async create(createBillDto: CreateBillDto) {
    const employee = await this.employeeRepository.findOneBy({
      id: createBillDto.employeeId,
    });
    const bill = new Bill();
    bill.name = createBillDto.name;
    bill.amount = 0;
    bill.total = 0;
    bill.buy = createBillDto.buy;
    bill.change = 0;
    bill.employee = employee;
    await this.billRepository.save(bill);

    for (const bills of createBillDto.billDetails) {
      const billDetails = new Billdetail();
      billDetails.amount = bills.amount;
      billDetails.material = await this.materialRepository.findOneBy({
        id: bills.materialId,
      });
      billDetails.name = billDetails.material.name;
      billDetails.price = billDetails.material.price_per_unit;
      billDetails.total =
        billDetails.material.price_per_unit * billDetails.amount;
      billDetails.bill = bill;
      await this.billDetailRepository.save(billDetails);

      const material = billDetails.material;
      material.quantity += bills.amount;
      await this.materialRepository.save(material);
    }
    await this.billRepository.save(bill);
    return await this.billRepository.findOne({
      where: { id: bill.id },
      relations: ['employee'],
    });
  }

  findAll() {
    return this.billRepository.find({
      relations: ['employee'],
    });
  }

  findOne(id: number) {
    return this.billRepository.findOne({
      where: { id: id },
      relations: ['employee'],
    });
  }

  async update(id: number, updateBillDto: UpdateBillDto) {
    const bill = await this.billRepository.findOneBy({
      id: id,
    });
    if (!bill) {
      throw new NotFoundException();
    }
    const updateBill = {
      ...bill,
      ...updateBillDto,
    };
    return this.billRepository.save(updateBill);
  }

  async remove(id: number) {
    const bill = await this.billRepository.findOneBy({
      id: id,
    });
    if (!bill) {
      throw new NotFoundException();
    }
    return this.billRepository.softRemove(bill);
  }
}
