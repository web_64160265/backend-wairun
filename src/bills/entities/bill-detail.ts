import { Material } from 'src/material/entities/material.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Bill } from './bill.entity';

@Entity()
export class Billdetail {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  name: string;
  @Column()
  amount: number;
  @Column({ type: 'float' })
  price: number;
  @Column({ type: 'float' })
  total: number;

  @CreateDateColumn()
  date: Date;
  @UpdateDateColumn()
  updatedDate: Date;
  @DeleteDateColumn()
  deletedDate: Date;

  @ManyToOne(() => Bill, (bill) => bill.billDetails)
  bill: Bill;

  @ManyToOne(() => Material, (material) => material.billdetails)
  material: Material;
}
