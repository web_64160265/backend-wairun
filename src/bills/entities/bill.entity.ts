import { User } from 'src/users/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Billdetail } from './bill-detail';

@Entity()
export class Bill {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;
  @Column()
  amount: number;
  @Column({ type: 'float' })
  total: number;

  @Column({ type: 'float' })
  buy: number;

  @Column({ type: 'float' })
  change: number;

  @ManyToOne(() => User, (employee) => employee.bills)
  employee: User;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @DeleteDateColumn()
  deletedDate: Date;

  @OneToMany(() => Billdetail, (billdetail) => billdetail.bill)
  billDetails: Billdetail[];
}
