import { Module } from '@nestjs/common';
import { BillService } from './bills.service';
import { BillsController } from './bills.controller';
import { Bill } from './entities/bill.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Billdetail } from './entities/bill-detail';
import { Material } from 'src/material/entities/material.entity';
import { User } from 'src/users/entities/user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Bill, User, Billdetail, Material])],
  controllers: [BillsController],
  providers: [BillService],
})
export class BillsModule {}
