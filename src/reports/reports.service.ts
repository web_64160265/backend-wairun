import { Injectable, Logger } from '@nestjs/common';
import { CreateReportDto } from './dto/create-report.dto';
import { UpdateReportDto } from './dto/update-report.dto';
import { InjectDataSource, InjectRepository } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { Controller, Get } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';

@Injectable()
export class ReportsService {
  private readonly logger = new Logger();
  constructor(@InjectDataSource() private DataSource: DataSource) {}
  @Cron(CronExpression.EVERY_10_HOURS)
  async tryToRun() {
    console.log('update');
    return await this.DataSource.query(
      'REPLACE INTO FactAgg(Fact_quantity_sold_agg, Fact_total_sales_agg, Fact_discount_agg, Month_id, User_id, Store_id, Customer_id, Product_id) SELECT SUM(Fact_quantity_sold), SUM(Fact_total_sales), SUM(Fact_discount), (SELECT Month_id FROM FactAgg WHERE time_dw.YEAR = time_dw.year AND time_dw.month = time_dw.month LIMIT 1) AS Month_id, User_id, Store_id, Customer_id, Product_id FROM FactDw AS FactDw INNER JOIN time_dw AS time_dw ON FactDw.Time_id = time_dw.time_id GROUP BY User_id, Store_id, Customer_id, Product_id, Month_id',
    );
  }
  handleCron() {
    this.logger.debug('Called every 2 HOUR');
  }

  async getBestOrderChart() {
    const query =
      'SELECT receiptdetail.name, SUM(receiptdetail.amount) AS bestseller FROM receiptdetail GROUP BY receiptdetail.name, MONTH(receiptdetail.createdDate) ORDER BY bestseller DESC LIMIT 5;';
    const result = await this.DataSource.query(query);
    return result;
  }

  async getDayofWeektotalamount() {
    const query =
      "SELECT DAYNAME(date) AS sales_day, COUNT(*) AS total_products_sold FROM receipt INNER JOIN receiptdetail ON receipt.id = receiptdetail.receiptId WHERE DATE(date) BETWEEN DATE_SUB(NOW(), INTERVAL 7 DAY) AND NOW() GROUP BY sales_day ORDER BY FIELD(sales_day, 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday')";
    const result = await this.DataSource.query(query);
    return result;
  }

  async getDayofweekTotalsales() {
    const query =
      "SELECT CASE WHEN DAYOFWEEK(receipt.date) = 1 THEN 'Sunday' WHEN DAYOFWEEK(receipt.date) = 2 THEN 'Monday' WHEN DAYOFWEEK(receipt.date) = 3 THEN 'Tuesday' WHEN DAYOFWEEK(receipt.date) = 4 THEN 'Wednesday' WHEN DAYOFWEEK(receipt.date) = 5 THEN 'Thursday' WHEN DAYOFWEEK(receipt.date) = 6 THEN 'Friday' WHEN DAYOFWEEK(receipt.date) = 7 THEN 'Saturday' END AS day_of_week, SUM(receiptdetail.total) AS total_sales FROM receipt INNER JOIN receiptdetail ON receipt.id = receiptdetail.receiptId WHERE DATE(receipt.date) BETWEEN DATE_SUB(NOW(), INTERVAL 1 YEAR) AND NOW() GROUP BY day_of_week ORDER BY FIELD(day_of_week, 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday')";
    const result = await this.DataSource.query(query);
    return result;
  }

  getCustomerByPoint() {
    return this.DataSource.query('SELECT * FROM customer_info_point');
  }
  getTotalsalebyday() {
    return this.DataSource.query('SELECT * FROM `getTotalsalebyday`');
  }
  getReceipt_info() {
    return this.DataSource.query('SELECT * FROM `getReceipt_info`');
  }
  getCustomer_info() {
    return this.DataSource.query('SELECT * FROM `getCustomer_info`');
  }

  getReportCustomer() {
    return this.DataSource.query('CALL `getReportCustomer`()');
  }
  getProduct() {
    return this.DataSource.query('CALL `getProduct`()');
  }

  getProduct2(searchCatagory: any) {
    return this.DataSource.query('CALL getProduct2(?)', [searchCatagory]);
  }

  getProductBySearchText(searchText: string) {
    return this.DataSource.query('SELECT * FROM product WHERE name LIKE ?', [
      `%${searchText}%`,
    ]);
  }
}
