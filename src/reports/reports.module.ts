import { Module } from '@nestjs/common';
import { ReportsService } from './reports.service';
import { ReportsController } from './reports.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Receiptdetail } from 'src/receipts/entities/receipt-detail';

@Module({
  // imports: [TypeOrmModule.forFeature([Receiptdetail, ReportsModule])],
  controllers: [ReportsController],
  providers: [ReportsService],
})
export class ReportsModule {}
