import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { ReportsService } from './reports.service';
import { CreateReportDto } from './dto/create-report.dto';
import { UpdateReportDto } from './dto/update-report.dto';

@Controller('reports')
export class ReportsController {
  constructor(private readonly reportsService: ReportsService) {}

  @Get('/recieptdetail/bestseller/chart')
  getBestOrderChart() {
    return this.reportsService.getBestOrderChart();
  }

  @Get('/recieptdetail/dayofweektotalamount/chart')
  getDayofWeektotalamount() {
    return this.reportsService.getDayofWeektotalamount();
  }

  @Get('/customer/bypoint')
  getCustomerByPoint() {
    return this.reportsService.getCustomerByPoint();
  }
  @Get('/customers/getCustomer')
  getReportCustomer() {
    return this.reportsService.getReportCustomer();
  }

  @Get('/receipts/getReceipt_info')
  getReceipt_info() {
    return this.reportsService.getReceipt_info();
  }
  @Get('/recieptdetail/dayofweektotalsale/chart')
  getDayofweekTotalsales() {
    return this.reportsService.getDayofweekTotalsales();
  }

  @Get('/customers/getCustomer_info')
  getCustomer_info() {
    return this.reportsService.getCustomer_info();
  }

  @Get('/view/getTotalsalebyday')
  getTotalsalebyday() {
    return this.reportsService.getTotalsalebyday();
  }

  @Get('/products/getProduct')
  getProduct() {
    return this.reportsService.getProduct();
  }

  @Get('/product/getProduct2')
  getProduct2(
    @Query()
    query: {
      searchCatagory?: string;
    },
  ) {
    {
      console.log(query);
      if (query.searchCatagory) {
        return this.reportsService.getProduct2(query.searchCatagory);
      }
    }
  }
}
