import { IsNotEmpty } from 'class-validator';
export class CreateCheckinoutDto {
  time_out: number;
  @IsNotEmpty()
  employeeId: number;
}
