import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { CheckinoutsService } from './checkinouts.service';
import { CreateCheckinoutDto } from './dto/create-checkinout.dto';
import { UpdateCheckinoutDto } from './dto/update-checkinout.dto';

@Controller('checkinout')
export class CheckinoutController {
  constructor(private readonly checkinoutService: CheckinoutsService) {}

  @Post()
  create(@Body() createCheckinoutDto: CreateCheckinoutDto) {
    return this.checkinoutService.create(createCheckinoutDto);
  }

  @Get()
  findAll() {
    return this.checkinoutService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.checkinoutService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string) {
    return this.checkinoutService.update(+id);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.checkinoutService.remove(+id);
  }
}
