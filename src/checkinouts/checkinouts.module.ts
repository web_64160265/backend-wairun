import { Module } from '@nestjs/common';
import { CheckinoutsService } from './checkinouts.service';
import { CheckinoutController } from './checkinouts.controller';

import { Summarysalary } from 'src/summarysalarys/entities/summarysalary.entity';
import { Checkinout } from './entities/checkinout.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/users/entities/user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([User, Summarysalary, Checkinout])],
  controllers: [CheckinoutController],
  providers: [CheckinoutsService],
})
export class CheckinoutsModule {}
