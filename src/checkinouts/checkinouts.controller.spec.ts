import { Test, TestingModule } from '@nestjs/testing';
import { CheckinoutController } from './checkinouts.controller';
import { CheckinoutsService } from './checkinouts.service';

describe('CheckinoutsController', () => {
  let controller: CheckinoutController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CheckinoutController],
      providers: [CheckinoutsService],
    }).compile();

    controller = module.get<CheckinoutController>(CheckinoutController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
