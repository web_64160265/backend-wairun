import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateCheckinoutDto } from './dto/create-checkinout.dto';
import { UpdateCheckinoutDto } from './dto/update-checkinout.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Checkinout } from './entities/checkinout.entity';
import { User } from 'src/users/entities/user.entity';
import { Summarysalary } from 'src/summarysalarys/entities/summarysalary.entity';
import { Repository } from 'typeorm';

@Injectable()
export class CheckinoutsService {
  constructor(
    @InjectRepository(Checkinout)
    private checkinoutsRepository: Repository<Checkinout>,
    @InjectRepository(User)
    private employeesRepository: Repository<User>,
    @InjectRepository(Summarysalary)
    private summarysalarysRepository: Repository<Summarysalary>,
  ) {}
  async create(createCheckinoutDto: CreateCheckinoutDto) {
    const employee = await this.employeesRepository.findOne({
      where: {
        id: createCheckinoutDto.employeeId,
      },
    });
    console.log(employee);
    const checkinout = new Checkinout();
    checkinout.time_in = new Date();
    console.log('timein', checkinout.time_in);
    checkinout.time_out = null;
    checkinout.employee = employee;
    await this.checkinoutsRepository.save(checkinout);
    try {
      const exitings = await this.summarysalarysRepository.findOne({
        where: { checkinout: { employee: { id: checkinout.employee.id } } },
      });
      console.log(exitings);
      if (!exitings) {
        const summarySalary = new Summarysalary();
        checkinout.summarysalaryId = summarySalary.id;
        summarySalary.date = new Date();
        summarySalary.work_hour = checkinout.total_hour;
        summarySalary.salary =
          checkinout.employee.hourly_wage * checkinout.total_hour;
        summarySalary.employee = checkinout.employee;
        const newSummarySalary = await this.summarysalarysRepository.save(
          summarySalary,
        );
        console.log('summarySalary:', summarySalary);
        console.log('summarySalary:', summarySalary);
        checkinout.summarysalary = newSummarySalary;
      } else {
        exitings.work_hour += checkinout.total_hour;
        exitings.salary +=
          checkinout.employee.hourly_wage * checkinout.total_hour;
        const updatedSummarySalary = await this.summarysalarysRepository.save(
          exitings,
        );
        checkinout.summarysalary = updatedSummarySalary;
      }
      console.log(checkinout);

      return await this.checkinoutsRepository.save(checkinout);
    } catch (e) {
      console.log(e);
    }
  }

  findAll() {
    return this.checkinoutsRepository.find({
      relations: ['employee', 'summarysalary'],
    });
  }

  async findOne(id: number) {
    const checkinout = await this.checkinoutsRepository.findOne({
      where: { id: id },
      relations: ['employee', 'summarysalary'],
    });
    if (!checkinout) {
      throw new NotFoundException();
    }
    return checkinout;
  }

  async update(id: number) {
    const checkinout = await this.checkinoutsRepository.findOne({
      where: { id: id },
    });
    console.log(checkinout);
    if (!checkinout || !checkinout.time_in) {
      throw new NotFoundException();
    }

    if (!checkinout.time_out) {
      checkinout.time_out = new Date();
    }
    //callwh
    if (checkinout.time_in && checkinout.time_out) {
      const timeInMs = checkinout.time_in.getTime();
      const timeOutMs = checkinout.time_out.getTime();
      const workHourMs = timeOutMs - timeInMs;
      const workHour = Math.abs(workHourMs / (1000 * 60 * 60));
      checkinout.total_hour = workHour;
      console.log('Total hour check in out', checkinout.total_hour);
      await this.checkinoutsRepository.save(checkinout);
      //update summary
      try {
        const sumsa = await this.summarysalarysRepository.findOne({
          where: { id: checkinout.summarysalaryId },
        });
        const emp = await this.employeesRepository.findOne({
          where: { id: checkinout.employeeId },
        });
        console.log('epm salary', emp.hourly_wage);
        console.log('eiei', sumsa);
        console.log('total hour', checkinout.total_hour);
        sumsa.work_hour = sumsa.work_hour + checkinout.total_hour;
        console.log('work hour:', sumsa.work_hour);
        sumsa.salary = emp.hourly_wage * sumsa.work_hour + sumsa.salary;
        return await this.summarysalarysRepository.save(sumsa);
      } catch (e) {
        console.log(e);
      }
    }
    return await this.checkinoutsRepository.save(checkinout);
  }

  async remove(id: number) {
    const checkinout = await this.checkinoutsRepository.findOneBy({
      id: id,
    });
    if (!checkinout) {
      throw new NotFoundException();
    }
    return this.checkinoutsRepository.softRemove(checkinout);
  }
}
