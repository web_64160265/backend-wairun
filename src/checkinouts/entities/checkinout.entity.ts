import { Summarysalary } from 'src/summarysalarys/entities/summarysalary.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
  ManyToOne,
  Entity,
} from 'typeorm';

@Entity()
export class Checkinout {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn({ type: 'datetime' })
  time_in: Date;

  @Column({ type: 'datetime', default: null })
  time_out: Date;

  @Column({ default: 0, type: 'float' })
  total_hour: number;

  @Column()
  employeeId: number;

  @Column({ default: null })
  summarysalaryId: number;

  @DeleteDateColumn()
  deletedDate: Date;

  @ManyToOne(() => User, (employee) => employee.checkinout)
  employee: User;

  @ManyToOne(() => Summarysalary, (summarysalary) => summarysalary.checkinout)
  summarysalary: Summarysalary;
}
