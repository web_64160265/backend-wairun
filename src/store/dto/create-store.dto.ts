import { IsNotEmpty, Length } from 'class-validator';

export class CreateStoreDto {
  @IsNotEmpty()
  @Length(3, 32)
  name: string;

  @IsNotEmpty()
  address_state: string;

  @IsNotEmpty()
  address_city: string;

  @IsNotEmpty()
  @Length(10)
  tel: string;
}
