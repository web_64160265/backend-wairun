import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateStoreDto } from './dto/create-store.dto';
import { UpdateStoreDto } from './dto/update-store.dto';
import { Store } from './entities/store.entity';

@Injectable()
export class StoreService {
  constructor(
    @InjectRepository(Store)
    private storesRepository: Repository<Store>,
  ) {}

  create(createStoreDto: CreateStoreDto) {
    return this.storesRepository.save(createStoreDto);
  }

  findAll(option) {
    return this.storesRepository.find(option);
  }

  findOne(id: number) {
    return this.storesRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateStoreDto: UpdateStoreDto) {
    try {
      const updatedStore = await this.storesRepository.save({
        id,
        ...updateStoreDto,
      });
      return updatedStore;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const store = await this.storesRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedStore = await this.storesRepository.remove(store);
      return deletedStore;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
