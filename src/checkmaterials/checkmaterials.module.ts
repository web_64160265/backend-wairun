import { Module } from '@nestjs/common';
import { CheckmaterialsService } from './checkmaterials.service';
import { CheckmaterialsController } from './checkmaterials.controller';
import { Checkmaterial } from './entities/checkmaterial.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

import { CheckmaterialDetail } from './entities/checkmaterial-details';
import { Material } from 'src/material/entities/material.entity';
import { User } from 'src/users/entities/user.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Checkmaterial,
      User,
      CheckmaterialDetail,
      Material,
    ]),
  ],
  controllers: [CheckmaterialsController],
  providers: [CheckmaterialsService],
})
export class CheckmaterialsModule {}
