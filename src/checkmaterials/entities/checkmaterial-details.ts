import { Material } from 'src/material/entities/material.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToOne,
} from 'typeorm';
import { Checkmaterial } from './checkmaterial.entity';
@Entity()
export class CheckmaterialDetail {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  qty_last: number;

  @Column({ default: 0 })
  qty_remain: number;

  @Column({ default: 0 })
  qty_expire: number;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @DeleteDateColumn()
  deletedDate: Date;

  @ManyToOne(() => Material, (material) => material.checkmatdetails)
  material: Material;

  @ManyToOne(() => Checkmaterial, (checkmat) => checkmat.checkmatdetails)
  checkmat: Checkmaterial;
}
