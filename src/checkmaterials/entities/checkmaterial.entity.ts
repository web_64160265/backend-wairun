import {
  Entity,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
} from 'typeorm';
import { CheckmaterialDetail } from './checkmaterial-details';
import { User } from 'src/users/entities/user.entity';
@Entity()
export class Checkmaterial {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @DeleteDateColumn()
  deletedDate: Date;

  @ManyToOne(() => User, (employee) => employee.checkmats)
  employee: User;

  @OneToMany(
    () => CheckmaterialDetail,
    (checkmatdetail) => checkmatdetail.checkmat,
  )
  checkmatdetails: CheckmaterialDetail[];
}
