import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { CheckmaterialsService } from './checkmaterials.service';
import { CreateCheckmaterialDto } from './dto/create-checkmaterial.dto';
import { UpdateCheckmaterialDto } from './dto/update-checkmaterial.dto';

@Controller('checkmat')
export class CheckmaterialsController {
  constructor(private readonly checkmaterialsService: CheckmaterialsService) {}

  @Post()
  create(@Body() createCheckmaterialDto: CreateCheckmaterialDto) {
    return this.checkmaterialsService.create(createCheckmaterialDto);
  }

  @Get()
  findAll() {
    return this.checkmaterialsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.checkmaterialsService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateCheckmaterialDto: UpdateCheckmaterialDto,
  ) {
    return this.checkmaterialsService.update(+id, updateCheckmaterialDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.checkmaterialsService.remove(+id);
  }
}
