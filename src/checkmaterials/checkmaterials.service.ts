import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateCheckmaterialDto } from './dto/create-checkmaterial.dto';
import { UpdateCheckmaterialDto } from './dto/update-checkmaterial.dto';
import { Checkmaterial } from './entities/checkmaterial.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { CheckmaterialDetail } from './entities/checkmaterial-details';
import { Material } from 'src/material/entities/material.entity';
import { User } from 'src/users/entities/user.entity';

@Injectable()
export class CheckmaterialsService {
  constructor(
    @InjectRepository(Checkmaterial)
    private checkmatsRepository: Repository<Checkmaterial>,
    @InjectRepository(CheckmaterialDetail)
    private checkmatdetail: Repository<CheckmaterialDetail>,
    @InjectRepository(User)
    private employeesRepository: Repository<User>,
    @InjectRepository(Material)
    private materialsRepository: Repository<Material>,
  ) {}
  async create(createCheckMaterialDto: CreateCheckmaterialDto) {
    const employee = await this.employeesRepository.findOneBy({
      id: createCheckMaterialDto.employeeId,
    });

    console.log(employee);
    const checkmaterial: Checkmaterial = new Checkmaterial();
    checkmaterial.employee = employee;

    await this.checkmatsRepository.save(checkmaterial);
    for (const check_mat of createCheckMaterialDto.checkmatdetails) {
      const checkMaterialDetail = new CheckmaterialDetail();
      checkMaterialDetail.material = await this.materialsRepository.findOneBy({
        id: check_mat.materialId,
      });

      checkMaterialDetail.name = checkMaterialDetail.material.name;
      checkMaterialDetail.qty_last = checkMaterialDetail.qty_remain =
        check_mat.qty_remain;
      checkMaterialDetail.qty_expire = check_mat.qty_expire;

      checkMaterialDetail.material.quantity = check_mat.qty_remain;
      await this.materialsRepository.save(checkMaterialDetail.material);

      checkMaterialDetail.checkmat = checkmaterial;
      await this.checkmatdetail.save(checkMaterialDetail);
    }
    await this.checkmatsRepository.save(checkmaterial);
    return await this.checkmatsRepository.findOne({
      where: { id: checkmaterial.id },
      relations: ['checkmatdetails'],
    });
  }

  findAll() {
    return this.checkmatsRepository.find({
      relations: ['employee'],
    });
  }

  findOne(id: number) {
    return this.checkmatsRepository.findOne({
      where: { id: id },
      relations: ['employee'],
    });
  }
  async update(id: number, updateMaterialDto: UpdateCheckmaterialDto) {
    const check_material = await this.checkmatsRepository.findOneBy({
      id: id,
    });
    if (!check_material) {
      throw new NotFoundException();
    }

    const updateMaterial = {
      ...check_material,
      ...updateMaterialDto,
    };
    return this.checkmatsRepository.save(updateMaterial);
  }

  async remove(id: number) {
    const check_material = await this.checkmatsRepository.findOneBy({
      id: id,
    });
    if (!check_material) {
      throw new NotFoundException();
    }
    return this.checkmatsRepository.softRemove(check_material);
  }
}
