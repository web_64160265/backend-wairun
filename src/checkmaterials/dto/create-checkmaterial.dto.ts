import { IsNotEmpty } from 'class-validator';

class CreateCheckmaterialDetailDto {
  @IsNotEmpty()
  materialId: number;

  @IsNotEmpty()
  qty_remain: number;

  @IsNotEmpty()
  qty_expire: number;
}
export class CreateCheckmaterialDto {
  @IsNotEmpty()
  employeeId: number;

  @IsNotEmpty()
  checkmatdetails: CreateCheckmaterialDetailDto[];
}
