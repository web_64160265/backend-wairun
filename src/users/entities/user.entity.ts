import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  OneToOne,
  JoinColumn,
  OneToMany,
} from 'typeorm';

import { Receipt } from 'src/receipts/entities/receipt.entity';
import { Bill } from 'src/bills/entities/bill.entity';
import { Checkmaterial } from 'src/checkmaterials/entities/checkmaterial.entity';
import { Checkinout } from 'src/checkinouts/entities/checkinout.entity';
import { Summarysalary } from 'src/summarysalarys/entities/summarysalary.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: '32',
  })
  name: string;
  @Column({
    unique: true,
    length: '64',
  })
  email: string;
  @Column({
    length: '128',
  })
  password: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  @Column()
  address: string;

  @Column({
    length: '10',
  })
  tel: string;

  @Column()
  position: string;

  @Column()
  hourly_wage: number;
  @OneToMany(() => Receipt, (receipt) => receipt.employee)
  receipts: Receipt[];

  @OneToMany(() => Bill, (bill) => bill.employee)
  bills: Bill[];

  @OneToMany(() => Checkmaterial, (checkmat) => checkmat.employee)
  checkmats: Checkmaterial[];

  @OneToMany(() => Checkinout, (checkinout) => checkinout.employee)
  checkinout: Checkinout[];

  @OneToMany(() => Summarysalary, (summarysalary) => summarysalary.employee)
  summarysalary: Summarysalary[];
}
