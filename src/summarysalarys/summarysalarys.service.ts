import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateSummarysalaryDto } from './dto/create-summarysalary.dto';
import { UpdateSummarysalaryDto } from './dto/update-summarysalary.dto';
import { Summarysalary } from './entities/summarysalary.entity';
import { Checkinout } from 'src/checkinouts/entities/checkinout.entity';
import { User } from 'src/users/entities/user.entity';

@Injectable()
export class SummarysalarysService {
  constructor(
    @InjectRepository(Summarysalary)
    private summarysalarysRepository: Repository<Summarysalary>,
    @InjectRepository(Checkinout)
    private checkinoutsRepository: Repository<Checkinout>,
    @InjectRepository(User)
    private employeesRepository: Repository<User>,
  ) {}

  // create(createSummarysalaryDto: CreateSummarysalaryDto) {
  //   return this.summarysalarysRepository.save(createSummarysalaryDto);
  // }

  findAll() {
    return this.summarysalarysRepository.find({
      relations: ['checkinout', 'employee'],
    });
  }

  async findOne(id: number) {
    const summarysalary = await this.summarysalarysRepository.findOne({
      where: { id: id },
    });
    if (!summarysalary) {
      throw new NotFoundException();
    }
    return summarysalary;
  }

  async update(id: number, updateSummarysalaryDto: UpdateSummarysalaryDto) {
    const summarysalary = await this.summarysalarysRepository.findOneBy({
      id: id,
    });
    if (!summarysalary) {
      throw new NotFoundException();
    }
    const updateSummarysalary = { ...summarysalary, ...updateSummarysalaryDto };
    return this.summarysalarysRepository.save(updateSummarysalary);
  }

  async remove(id: number) {
    const summarysalary = await this.summarysalarysRepository.findOneBy({
      id: id,
    });
    if (!summarysalary) {
      throw new NotFoundException();
    }
    return this.summarysalarysRepository.softRemove(summarysalary);
  }
}
