import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { SummarysalarysService } from './summarysalarys.service';
import { CreateSummarysalaryDto } from './dto/create-summarysalary.dto';
import { UpdateSummarysalaryDto } from './dto/update-summarysalary.dto';

@Controller('summarysalarys')
export class SummarysalarysController {
  constructor(private readonly summarysalarysService: SummarysalarysService) {}

  // @Post()
  // create(@Body() createSummarysalaryDto: CreateSummarysalaryDto) {
  //   return this.summarysalarysService.create(createSummarysalaryDto);
  // }

  @Get()
  findAll() {
    return this.summarysalarysService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.summarysalarysService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateSummarysalaryDto: UpdateSummarysalaryDto,
  ) {
    return this.summarysalarysService.update(+id, updateSummarysalaryDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.summarysalarysService.remove(+id);
  }
}
