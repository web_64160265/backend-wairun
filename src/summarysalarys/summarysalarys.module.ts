import { Module } from '@nestjs/common';
import { SummarysalarysService } from './summarysalarys.service';
import { SummarysalarysController } from './summarysalarys.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Summarysalary } from './entities/summarysalary.entity';
import { Checkinout } from 'src/checkinouts/entities/checkinout.entity';
import { User } from 'src/users/entities/user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Summarysalary, Checkinout, User])],
  controllers: [SummarysalarysController],
  providers: [SummarysalarysService],
})
export class SummarysalarysModule {}
