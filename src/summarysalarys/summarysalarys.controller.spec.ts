import { Test, TestingModule } from '@nestjs/testing';
import { SummarysalarysController } from './summarysalarys.controller';
import { SummarysalarysService } from './summarysalarys.service';

describe('SummarysalarysController', () => {
  let controller: SummarysalarysController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SummarysalarysController],
      providers: [SummarysalarysService],
    }).compile();

    controller = module.get<SummarysalarysController>(SummarysalarysController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
