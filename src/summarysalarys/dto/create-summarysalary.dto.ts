import { IsNotEmpty } from 'class-validator';

export class CreateSummarysalaryDto {
  @IsNotEmpty()
  work_hour: number;
  @IsNotEmpty()
  salary: number;
  summarysalaryId: number;
}
