import { Checkinout } from 'src/checkinouts/entities/checkinout.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Summarysalary {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  date: Date;

  @Column({ type: 'float', default: 0 })
  work_hour: number;

  @Column({ type: 'float', default: 0 })
  salary: number;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @DeleteDateColumn()
  deletedDate: Date;

  @OneToMany(() => Checkinout, (checkinout) => checkinout.summarysalary)
  checkinout: Checkinout[];

  @ManyToOne(() => User, (employee) => employee.summarysalary)
  employee: User;
}
