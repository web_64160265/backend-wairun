import { Billdetail } from 'src/bills/entities/bill-detail';
import { CheckmaterialDetail } from 'src/checkmaterials/entities/checkmaterial-details';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
} from 'typeorm';
@Entity()
export class Material {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  min_quantity: number;

  @Column()
  quantity: number;

  @Column()
  unit: string;

  @Column()
  price_per_unit: number;

  @OneToMany(() => Billdetail, (billdetail) => billdetail.material)
  billdetails: Billdetail[];

  @OneToMany(
    () => CheckmaterialDetail,
    (checkmatdetail) => checkmatdetail.material,
  )
  checkmatdetails: CheckmaterialDetail[];

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}
