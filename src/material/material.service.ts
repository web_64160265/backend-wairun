import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateMaterialDto } from './dto/create-material.dto';
import { UpdateMaterialDto } from './dto/update-material.dto';
import { Material } from './entities/material.entity';
import { FindManyOptions, Like, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class MaterialService {
  constructor(
    @InjectRepository(Material)
    private materialsRepository: Repository<Material>,
  ) {}
  create(createMaterialDto: CreateMaterialDto) {
    return this.materialsRepository.save(createMaterialDto);
  }

  async findAll(query): Promise<Paginate> {
    const keyword = query.keyword || '';
    const orderBy = query.orderBy || 'name';
    const order = query.order || 'ASC';
    const [result, total] = await this.materialsRepository.findAndCount({
      where: { name: Like(`%${keyword}%`) },
      order: { [orderBy]: order },
    });
    return {
      data: result,
      count: total,
    };
  }

  async findOne(id: number) {
    const material = await this.materialsRepository.findOne({
      where: { id: id },
      relations: ['material'],
    });
    if (!material) {
      throw new NotFoundException();
    }
    return material;
  }

  async update(id: number, updateMaterialDto: UpdateMaterialDto) {
    const material = await this.materialsRepository.findOne({
      where: { id: id },
    });
    if (!material) {
      throw new NotFoundException();
    }
    const updatedMaterial = { ...material, ...updateMaterialDto };
    return this.materialsRepository.save(updatedMaterial);
  }

  async remove(id: number) {
    const material = await this.materialsRepository.findOneBy({ id: id });
    if (!material) {
      throw new NotFoundException();
    }
    return this.materialsRepository.softRemove(material);
  }
}
