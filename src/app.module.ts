import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CustomersModule } from './customers/customers.module';
import { Customer } from './customers/entities/customer.entity';
import { Product } from './products/entities/product.entity';
import { ProductsModule } from './products/products.module';
import { ReceiptsModule } from './receipts/receipts.module';
import { Receipt } from './receipts/entities/receipt.entity';
import { Receiptdetail } from './receipts/entities/receipt-detail';
import { UsersModule } from './users/users.module';
import { User } from './users/entities/user.entity';
import { CategoryModule } from './category/category.module';

import { Category } from './category/entities/category.entity';
import { Store } from './store/entities/store.entity';
import { StoreModule } from './store/store.module';
import { AuthModule } from './auth/auth.module';
import { BillsModule } from './bills/bills.module';
import { Bill } from './bills/entities/bill.entity';
import { CheckmaterialsModule } from './checkmaterials/checkmaterials.module';
import { Checkmaterial } from './checkmaterials/entities/checkmaterial.entity';
import { MaterialModule } from './material/material.module';
import { Material } from './material/entities/material.entity';
import { Billdetail } from './bills/entities/bill-detail';
import { Summarysalary } from './summarysalarys/entities/summarysalary.entity';
import { SummarysalarysModule } from './summarysalarys/summarysalarys.module';
import { CheckmaterialDetail } from './checkmaterials/entities/checkmaterial-details';
import { CheckinoutsModule } from './checkinouts/checkinouts.module';
import { Checkinout } from './checkinouts/entities/checkinout.entity';
import { ReportsModule } from './reports/reports.module';
// import { ScheduleModule } from '@nestjs/schedule';
@Module({
  imports: [
    // ScheduleModule.forRoot(),
    TypeOrmModule.forRoot(
      //   {
      //   type: 'sqlite',
      //   database: 'db.sqlite',
      //   synchronize: true,
      //   migrations: [],
      //   entities: [
      //     Customer,
      //     Product,
      //     Receipt,
      //     Receiptdetail,
      //     User,
      //     Employee,
      //     Category,
      //     Store,
      //   ],
      // }
      {
        type: 'mysql',
        host: 'angsila.informatics.buu.ac.th',
        port: 3306,
        username: 'guest10',
        password: 'MZXdWFAR',
        database: 'guest10',
        entities: [
          Customer,
          Product,
          Receipt,
          Receiptdetail,
          User,

          Category,
          Store,
          Bill,
          Checkmaterial,
          Material,
          Billdetail,
          Summarysalary,
          CheckmaterialDetail,
          Checkinout,
        ],
        synchronize: true,
      },
    ),

    CustomersModule,
    ProductsModule,
    ReceiptsModule,
    UsersModule,
    CategoryModule,
    StoreModule,
    AuthModule,
    BillsModule,
    CheckmaterialsModule,
    MaterialModule,
    SummarysalarysModule,
    CheckinoutsModule,
    ReportsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
