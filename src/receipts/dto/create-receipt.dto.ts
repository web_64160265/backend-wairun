import { IsNotEmpty } from 'class-validator';

class CreateReceiptDetailDto {
  @IsNotEmpty()
  productId: number;

  @IsNotEmpty()
  amount: number;
}

export class CreateReceiptDto {
  customerId: number;

  @IsNotEmpty()
  storeId: number;

  @IsNotEmpty()
  employeeId: number;

  @IsNotEmpty()
  receiptdetails: CreateReceiptDetailDto[];

  @IsNotEmpty()
  received: number;

  @IsNotEmpty()
  discount: number;

  @IsNotEmpty()
  payment: string;
}
