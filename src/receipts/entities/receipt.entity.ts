import { Customer } from 'src/customers/entities/customer.entity';

import { Store } from 'src/store/entities/store.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { Receiptdetail } from './receipt-detail';
import { User } from 'src/users/entities/user.entity';
@Entity()
export class Receipt {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn()
  date: Date;

  @Column({ type: 'float' })
  discount: number;

  @Column({ type: 'float' })
  total: number;

  @Column({ type: 'float' })
  received: number;

  @Column({ type: 'float' })
  change: number;

  @Column()
  payment: string;

  @Column()
  amount: number;

  @UpdateDateColumn()
  updatedDate: Date;
  @DeleteDateColumn()
  deletedDate: Date;

  @OneToMany(() => Receiptdetail, (receiptdetail) => receiptdetail.receipt)
  receiptdetails: Receiptdetail[];

  @ManyToOne(() => Customer, (customer) => customer.receipts)
  customer: Customer;

  @ManyToOne(() => Store, (store) => store.receipts)
  store: Store;

  @ManyToOne(() => User, (employee) => employee.receipts)
  employee: User;
}
