import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { ReceiptsService } from './receipts.service';
import { CreateReceiptDto } from './dto/create-receipt.dto';
import { UpdateReceiptDto } from './dto/update-receipt.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Receiptdetail } from './entities/receipt-detail';

@Controller('receipts')
export class ReceiptsController {
  constructor(
    private readonly receiptsService: ReceiptsService,
    @InjectRepository(Receiptdetail)
    private receiptDetailRepository: Repository<Receiptdetail>,
  ) {}

  @Get('bestsellers')
  async getBestSellers() {
    const query = this.receiptDetailRepository
      .createQueryBuilder('receiptdetail')
      .select(['receiptdetail.name', 'SUM(receiptdetail.amount) AS bestseller'])
      .groupBy('receiptdetail.name, MONTH(receiptdetail.createdDate)')
      .orderBy('bestseller', 'DESC')
      .limit(5);

    const result = await query.getRawMany();
    return result;
  }
  @Post()
  create(@Body() createReceiptDto: CreateReceiptDto) {
    console.log(createReceiptDto);
    return this.receiptsService.create(createReceiptDto);
  }

  @Get()
  findAll() {
    return this.receiptsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.receiptsService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateReceiptDto: UpdateReceiptDto) {
    return this.receiptsService.update(+id, updateReceiptDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.receiptsService.remove(+id);
  }
}
