import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Customer } from 'src/customers/entities/customer.entity';

import { Product } from 'src/products/entities/product.entity';
import { Store } from 'src/store/entities/store.entity';
import { Repository } from 'typeorm';
import { CreateReceiptDto } from './dto/create-receipt.dto';
import { UpdateReceiptDto } from './dto/update-receipt.dto';
import { Receiptdetail } from './entities/receipt-detail';
import { Receipt } from './entities/receipt.entity';
import { User } from 'src/users/entities/user.entity';

@Injectable()
export class ReceiptsService {
  constructor(
    @InjectRepository(Receipt)
    private receiptsRepository: Repository<Receipt>,
    @InjectRepository(Customer)
    private customersRepository: Repository<Customer>,
    @InjectRepository(Product)
    private productsRepository: Repository<Product>,
    @InjectRepository(Store)
    private storesRepository: Repository<Store>,
    @InjectRepository(User)
    private employeesRepository: Repository<User>,
    @InjectRepository(Receiptdetail)
    private receiptItemsRepository: Repository<Receiptdetail>,
  ) {}

  async create(createReceiptDto: CreateReceiptDto) {
    const customer = await this.customersRepository.findOneBy({
      id: createReceiptDto.customerId,
    });
    const store = await this.storesRepository.findOneBy({
      id: createReceiptDto.storeId,
    });

    const employee = await this.employeesRepository.findOneBy({
      id: createReceiptDto.employeeId,
    });
    const receipt: Receipt = new Receipt();
    receipt.customer = customer;
    receipt.store = store;
    receipt.employee = employee;
    receipt.received = createReceiptDto.received;
    receipt.discount = createReceiptDto.discount;
    receipt.payment = createReceiptDto.payment;
    receipt.amount = 0;
    receipt.total = 0;
    receipt.change = 0;

    await this.receiptsRepository.save(receipt); // ได้ id

    for (const ad of createReceiptDto.receiptdetails) {
      const receiptdetail = new Receiptdetail();
      receiptdetail.amount = ad.amount;
      receiptdetail.product = await this.productsRepository.findOneBy({
        id: ad.productId,
      });

      receiptdetail.name = receiptdetail.product.name;
      receiptdetail.price = receiptdetail.product.price;
      receiptdetail.total = receiptdetail.price * receiptdetail.amount;
      receiptdetail.receipt = receipt; // อ้างกลับ
      await this.receiptItemsRepository.save(receiptdetail);
      receipt.amount = receipt.amount + receiptdetail.amount;
      receipt.total = receipt.total + receiptdetail.total;

      console.log(receiptdetail);
    }
    receipt.total = receipt.total - receipt.discount;
    receipt.change = receipt.received - receipt.total;
    await this.receiptsRepository.save(receipt); // ได้ id
    return await this.receiptsRepository.findOne({
      where: { id: receipt.id },
      relations: ['receiptdetails', 'customer', 'store', 'employee'],
    });
  }

  findAll() {
    return this.receiptsRepository.find({
      relations: ['receiptdetails', 'customer', 'store', 'employee'],
    });
  }

  async findOne(id: number) {
    const receipt = await this.receiptsRepository.findOne({
      where: { id: id },
      relations: ['customer', 'store', 'employee', 'receiptdetails'],
    });
    if (!receipt) {
      throw new NotFoundException();
    }
    return receipt;
  }

  update(id: number, updateReceiptDto: UpdateReceiptDto) {
    return `This action updates a #${id} order`;
  }

  async remove(id: number) {
    const order = await this.receiptsRepository.findOneBy({ id: id });
    return this.receiptsRepository.softRemove(order);
  }
}
