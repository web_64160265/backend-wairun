import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Category } from 'src/category/entities/category.entity';
import { Like, Repository } from 'typeorm';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

@Injectable()
export class ProductsService {
  productsPartialType: any;
  constructor(
    @InjectRepository(Product)
    private productsRepository: Repository<Product>,
    @InjectRepository(Category)
    private categorysRepository: Repository<Category>,
  ) {}

  async create(createProductDto: CreateProductDto) {
    const category = await this.categorysRepository.findOneBy({
      id: createProductDto.categoryId,
    });
    const product = new Product();
    product.name = createProductDto.name;
    product.type = createProductDto.type;
    product.size = createProductDto.size;
    product.price = createProductDto.price;
    product.image = createProductDto.image;
    product.category = category;
    await this.productsRepository.save(product);
    return this.productsRepository.findOne({
      where: { id: product.id },
      relations: ['category'],
    });
  }

  findByCategory(id: number) {
    return this.productsRepository.find({
      where: { categoryId: id },
    });
  }

  async findAll(query): Promise<Paginate> {
    // const cat = query.cat || 1; //ของอาจารย์catมันถูกใช้อาจจะต้องเปลี่ยนชื่อตัวแปรรึป่าว??
    const page = query.page || 1;
    const take = query.take || 1;
    const skip = (page - 1) * take;
    const keyword = query.keyword || '';
    const orderBy = query.orderBy || 'name';
    const order = query.order || 'ASC';
    const currentPage = page;
    const [result, total] = await this.productsRepository.findAndCount({
      relations: ['category'],
      where: { name: Like(`%${keyword}%`) },
      order: { [orderBy]: order },
      take: take,
      skip: skip,
    });
    const lastPage = Math.ceil(total / take);
    return {
      data: result,
      count: total,
    };
  }

  findOne(id: number) {
    return this.productsRepository.findOne({
      where: { id: id },
      relations: ['category'],
    });
  }

  async update(id: number, updateProductDto: UpdateProductDto) {
    console.log(updateProductDto);
    const product = await this.productsRepository.findOneBy({
      id,
    });
    if (!product) {
      throw new NotFoundException();
    }
    const updatedProduct = { ...product, ...updateProductDto };
    console.log(updateProductDto);
    return this.productsRepository.save(updatedProduct);
  }

  async remove(id: number) {
    const product = await this.productsRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedProduct = await this.productsRepository.remove(product);
      return deletedProduct;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
